��          �   %   �      `     a  
   g  
   r     }     �      �     �     �     �  
   �     �     �     �       
   '     2     C     H     X  =   d     �     �     �     �  W   �  6   #  9  Z     �     �     �  
   �     �  ?   �  <   6  6   s     �     �     �  7   �  /     3   L     �  4   �     �     �     �  �   
     �  
   �  %   �     �  �   �  d   �	           	                                     
                                                                            About Add preset Background Colors Delete preset Do you really want to delete {}? Export image as.. Export image... File Foreground Image Initial sequence Invalid preset name. Invalid presets file. Line width Name new preset: Plot Postscript file Preset name Preset name can not be empty or equal to existed preset name. Presets Quit Rainbow mode Recursion deep This script programm made just for fun
by Anton Karmanov (bergentroll@openmailbox.org). You need ghostscript to crop ps and convert it to png. Project-Id-Version: fractal-plotter
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-18 18:52+0500
PO-Revision-Date: 2016-08-23 18:34+0500
Last-Translator: Anton Karmanov <anton@anton-home>
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 О программе Добавить пресет Фон Цвета Удалить пресет Вы действительно хотите удалить {}? Экспортировать изображение как.. Экспортировать изображение... Файл Основной цвет Изображение Начальная последовательность Некорректное имя пресета. Некорректный файл пресетов. Ширина линии Название для нового пресета: Построить Файл Postscript Имя пресета Имя пресета не может быть пустым и не может повторять сушествующее имя. Пресеты Выход Режим раскрашивания Глубина рекурсии Данная скриповая пограмма написана исключительно для лулзов
Антоном Кармановым (bergentroll@openmailbox.org). Требуется ghostscript для обрезки ps-файла и конвертации в png. 